import os
from copy import deepcopy
import pickle

from moves import MoveSelector
from table_symmetries import TableSymmetries


def load_pickle(filename, default):
    if os.path.exists(filename):
        with open(filename, 'rb') as handle:
            return pickle.load(handle)
    return default


class Memory:
    def __init__(self, filename=None, reset=False):
        self.filename = filename
        if reset or filename is None:
            self.data = dict()
        else:
            self.data = load_pickle(filename, dict())

    def check_memory(self, table):
        if table not in self.data:
            new_tb = deepcopy(table)
            self.data[new_tb] = MoveSelector(new_tb)
        symmetries = TableSymmetries()
        _, permutations = symmetries.group_representative(table)
        return permutations[0], self.data[table].probability

    def update(self, move, result, learning_rate):
        symmetries = TableSymmetries()
        _, permutations = symmetries.group_representative(move.table)
        symmetric_moves = []
        for permutation in permutations:
            for i in range(9):
                if permutation[i] == move.position:
                    symmetric_moves.append(i)
                    break
        self.data[move.table].update(symmetric_moves, result, learning_rate)

    def save(self):
        if self.filename is None:
            print("No memory file provided")
        else:
            with open(self.filename, 'wb') as handle:
                pickle.dump(self.data, handle, protocol=pickle.HIGHEST_PROTOCOL)
