import config
from ai_player import AIPlayer
from game import Game
from moves import GameOutcome
from player import PlayerMark


class Learner:
    def __init__(self, iterations, learning_rate, memory):
        self.iterations = iterations
        self.learning_rate = learning_rate
        self.memory = memory

    def learn(self, verbose):
        pl1 = AIPlayer(self.memory, True, verbose)
        pl2 = AIPlayer(self.memory, True, verbose)
        games_ended_in_draw_cnt = 0
        for iter_cnt in range(self.iterations):
            if iter_cnt > 0 and iter_cnt % config.learning_iteration_len == 0:
                self.memory.save()
                percentage_draw = games_ended_in_draw_cnt / config.learning_iteration_len
                print(f"Iteration: {iter_cnt}\t\t\t\tDraw percentage: {percentage_draw * 100}%")
                games_ended_in_draw_cnt = 0
            game = Game(pl1, pl2)
            game.game_cycle()
            games_ended_in_draw_cnt += 0 == game.result

            augment_ratio = self.learning_rate
            for i in range(len(game.history) - 1, -1, -1):
                move = game.history[i]
                if 0 == game.result:
                    game_outcome = GameOutcome.DRAW
                elif game.result == PlayerMark.get_mark(move.player):
                    game_outcome = GameOutcome.WON
                else:
                    game_outcome = GameOutcome.LOST

                self.memory.update(move, game_outcome, augment_ratio)
                augment_ratio *= config.learning_ratio_decay
        self.memory.save()
