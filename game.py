from copy import deepcopy
import numpy as np

from moves import Move
from player import PlayerMark
from table import Table
from human_player import HumanPlayer


def expand_args(f):
    def inner(*args):
        if isinstance(args[0], np.ndarray):
            return f(*(args[0]))
        return f(*args)

    return inner


class Game:
    def __init__(self, player_1, player_2):
        self.table = Table()
        self.players = [player_1, player_2]
        self.history = list()
        self.result = None

    def check_for_win(self):
        for i in range(3):
            if (winner := self.check_line(self.table.fields[i])) > 0:
                return winner
            if (winner := self.check_line(self.table[:, i])) > 0:
                return winner
        if (winner := self.check_line(self.table[0, 0], self.table[1, 1], self.table[2, 2])) > 0:
            return winner
        winner = self.check_line(self.table[2, 0], self.table[1, 1], self.table[0, 2])
        return winner

    @staticmethod
    @expand_args
    def check_line(a, b, c):
        if a == b and b == c:
            return a
        return 0

    def game_end(self, winner):
        self.result = winner
        has_human_player = max([isinstance(pl, HumanPlayer) for pl in self.players])
        if not has_human_player:
            return
        print(self.table)
        if 0 == winner:
            print("Game ended in a draw")
        else:
            print(f"Congrats to player {winner}!")

    @staticmethod
    def get_other_player(player):
        return 1 - player

    def game_cycle(self):
        curr_player_idx = 0
        for _ in range(9):
            choice = self.players[curr_player_idx].prompt_action(self.table)
            self.history.append(Move(curr_player_idx, deepcopy(self.table), choice))
            self.table[choice] = PlayerMark.get_mark(curr_player_idx)
            winner = self.check_for_win()
            has_winner = winner > 0
            if has_winner:
                self.game_end(winner)
                return
            curr_player_idx = self.get_other_player(curr_player_idx)
        self.game_end(0)
