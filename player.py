from abc import abstractmethod


class PlayerMark:
    PLAYER_1 = 1
    PLAYER_2 = 2

    @staticmethod
    def get_mark(player):
        if player == 0:
            return PlayerMark.PLAYER_1
        if player == 1:
            return PlayerMark.PLAYER_2


class Player:
    player_cnt = 0

    def __init__(self):
        Player.player_cnt += 1

    @abstractmethod
    def prompt_action(self, table):
        pass
