#!/home/aantonov/projects/uni/TicTacToe/bin/python
from enum import Enum

from learn import Learner
from memory import Memory
import argparse
from game import Game
from human_player import HumanPlayer
from ai_player import AIPlayer


class GameMode(Enum):
    SINGLEPLAYER = 0
    MULTIPLAYER = 1
    LEARNING = 2


def game_mode_type(mode_as_string):
    try:
        return GameMode[mode_as_string.upper()]
    except KeyError:
        raise argparse.ArgumentTypeError("Unrecognised mode")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    mode = parser.add_argument("-m", "--mode"
                        , default='SINGLEPLAYER'
                        , help="Select mode single player / multiplayer / learning"
                        , type=game_mode_type)
    parser.add_argument("-lr", "--learning_rate"
                        , default=.01
                        , type=float
                        )
    parser.add_argument("-it", "--iterations"
                        , default=20000
                        , type=int
                        )
    parser.add_argument("-mem", "--memory"
                        , default="./base.mem"
                        , type=str
                        )
    parser.add_argument("-cl", "--clean_memory"
                        , action="store_true"
                        )
    parser.add_argument("-v", "--verbose"
                        , action="store_true"
                        )

    args = parser.parse_args()

    memory = Memory(args.memory, args.clean_memory)
    if args.mode == GameMode.LEARNING:
        learning_session = Learner(args.iterations, args.learning_rate, memory, )
        learning_session.learn(args.verbose)
    else:
        player_1 = HumanPlayer()
        player_2 = AIPlayer(memory, False, args.verbose) if GameMode.SINGLEPLAYER else HumanPlayer()
        game = Game(player_1, player_2)
        game.game_cycle()
