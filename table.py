import numpy as np
from table_symmetries import TableSymmetries


class Table:
    signs = [' ', 'X', 'O']

    string_template = """
    |<0>|<1>|<2>|
    +---+---+---+
    |<3>|<4>|<5>|
    +---+---+---+
    |<6>|<7>|<8>|
    """

    def __init__(self, fields=None):
        if fields is None:
            self.fields = np.zeros(shape=(3, 3), dtype=np.int)
        else:
            self.fields = fields

    def __repr__(self):
        as_string = Table.string_template
        for idx in range(9):
            sign = Table.signs[self[idx]]
            as_string = as_string.replace(f'<{idx}>', f" {sign} ")

        return as_string

    @staticmethod
    def index_decomposition(idx):
        if isinstance(idx, tuple):
            x, y = idx
        else:
            x = int(idx / 3)
            y = idx % 3
        return x, y

    def simple_hash(self, mask):
        digit = 1
        code = 0
        for i in range(9):
            code += digit * self[mask[i]]
            digit *= 3
        return code

    def __getitem__(self, idx):
        x, y = Table.index_decomposition(idx)
        return self.fields[x, y]

    def __setitem__(self, idx, value):
        x, y = Table.index_decomposition(idx)
        self.fields[x, y] = value

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __hash__(self):
        symmetries = TableSymmetries()
        encoding, _ = symmetries.group_representative(self)
        return encoding
