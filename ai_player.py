import numpy as np

from player import Player
from memory import Memory


class AIPlayer(Player):
    def __init__(self, memory=None, learning_mode=True, verbose=False):
        super().__init__()
        if memory is None:
            memory = Memory()
        self.memory = memory
        self.learning_mode = learning_mode
        self.verbose = verbose

    def prompt_action(self, table):
        order, prob = self.memory.check_memory(table)
        if self.verbose:
            print("hash: ", hash(table))
            from pprint import pprint
            print(f'order: {order}\nprob:')
            pprint(prob, indent=4)
        if self.learning_mode:
            return np.random.choice(order, p=prob)
        return order[np.argmax(prob)]
