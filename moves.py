from dataclasses import dataclass

import numpy as np
from enum import Enum

import config
from table import Table


class GameOutcome(Enum):
    DRAW = 0
    WON = 1
    LOST = 2


@dataclass
class Move:
    player: int
    table: Table
    position: int


class MoveSelector:
    def __init__(self, table):
        probability = np.ones(shape=table.fields.shape)
        probability[table.pivot().fields > 0] = 0
        probability /= np.sum(probability)
        self.probability = probability.reshape(-1)

    @staticmethod
    def calc_update_value(direction, learning_rate):
        update_amount = 0
        if GameOutcome.DRAW == direction:
            update_amount = learning_rate * config.draw_learning_rate_modifier

        if GameOutcome.WON == direction:
            update_amount = learning_rate

        if GameOutcome.LOST == direction:
            update_amount = - learning_rate
        return update_amount

    def update(self, symmetric_moves, direction, learning_rate):
        update_amount = MoveSelector.calc_update_value(direction, learning_rate)
        for position in symmetric_moves:
            self.probability[position] += update_amount
            self.probability[position] = max(config.probability_floor, self.probability[position])
        self.probability /= np.sum(self.probability)
