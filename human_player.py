from player import Player
from table import Table


def print_choices(table):
    prompt_string = Table.string_template.replace('<', ' ').replace('>', ' ')
    return f"{table}\nPlease enter your choice\n{prompt_string}"


class HumanPlayer(Player):
    def __init__(self):
        super().__init__()

    def prompt_action(self, table):
        while choice := input(print_choices(table)):
            choice = -1 if len(choice) != 1 or choice[0] < '0' or choice[0] > '8' else int(choice)
            if -1 != choice and 0 == table[choice]:
                return choice
            print("Invalid choice, please try again")
