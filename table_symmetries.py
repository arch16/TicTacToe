import numpy as np


class TableSymmetries(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(TableSymmetries, cls).__new__(cls)
            cls._instance.symmetries = []
            base = np.array(range(9)).reshape(3, 3)

            for _ in range(4):
                cls._instance.symmetries.append(base.reshape(-1))
                cls._instance.symmetries.append(np.transpose(base).reshape(-1))
                base = cls.rotate_90_degrees(base)

        return cls._instance

    @staticmethod
    def rotate_90_degrees(fields):
        rotated = np.concatenate([fields[2].reshape(3, 1), fields[1].reshape(3, 1), fields[0].reshape(3, 1)], axis=1)
        return rotated.reshape(3, 3)

    def group_representative(self, table):
        encodings = [table.simple_hash(mask) for mask in self._instance.symmetries]
        encoded = int(min(encodings))
        permutations = [self._instance.symmetries[i] for i in range(8) if encodings[i] == encoded]
        return encoded, permutations
